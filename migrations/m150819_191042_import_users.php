<?php

use yii\db\Schema;
use yii\db\Migration;

class m150819_191042_import_users extends Migration
{
    public function up()
    {
        echo "\nStart importing users\n";
        $usersFile = Yii::getAlias("@app") . "/users.json";
        if (!file_exists($usersFile)) {
            echo "\nFile for importing users not found!\n";
            return false;
        }
        $users = @json_decode(file_get_contents($usersFile), true);

        if (!array($users) || empty($users)) {
            echo "\nNothing to import\n";
            return false;
        }

        $columns = array_keys($users[0]);
        $rows = [];

        foreach ($users as $user) {
            array_push($rows, array_values($user));
        }

        $this->batchInsert("Users", $columns, $rows);

        return true;
    }

    public function down()
    {
        $this->delete("Users");
        echo "\nAll users deleted.\n";

        return true;
    }
}
