<?php

use yii\db\Schema;
use yii\db\Migration;

class m150819_191043_import_loans extends Migration
{
    public function up()
    {
        echo "\nStart importing loans\n";

        $dateColumns = ["dateApplied", "dateLoanEnds"];
        $loansFile = Yii::getAlias("@app") . "/loans.json";

        if (!file_exists($loansFile)) {
            echo "\nFile for importing loans not found!\n";
            return true;
        }
        $loans = @json_decode(file_get_contents($loansFile), true);

        if (!array($loans) || empty($loans)) {
            echo "\nNothing to import\n";
            return true;
        }

        $columns = array_keys($loans[0]);
        $rows = [];

        foreach ($loans as $loan) {
            foreach ($loan as $k => &$v) {
                if (in_array($k, $dateColumns)) {
                    $v = date("d-m-Y H:i:s", $v);
                }
            }
            array_push($rows, array_values($loan));
        }

        $this->execute("SET DATESTYLE TO ISO, DMY");
        $this->batchInsert("Loans", $columns, $rows);
        $this->execute("SET DATESTYLE TO 'default'");

        return true;
    }

    public function down()
    {
        $this->delete("Loans");
        echo "\nAll loans deleted.\n";

        return true;
    }
}
