<?php
use \yii\helpers\Url;

/* @var $this yii\web\View */
$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="body-content">

        <div class="row">
            <div class="col-lg-6">
                <p>
                    <a class="btn btn-default" href="<?=Url::to(["users/"])?>">Users &raquo;</a>
                </p>
                <p>
                    <a class="btn btn-default" href="<?=Url::to(["loans/"])?>">Loans &raquo;</a>
                </p>
            </div>
        </div>

    </div>
</div>
